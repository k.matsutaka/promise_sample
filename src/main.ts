// メイン実行
main();

/**
 * メイン関数
 */
function main() {
    let requests: RequestDammyModel[] = [];
    for (let i = 0; i < 10; i++) {
        const request: RequestDammyModel = {
            requestId: ('00000' + (i + 1)).slice(-5),
            waitTime: 1
        };
        requests.push(request);
    }
    // 実行
    adapterDammy(requests, (responses) => {
        console.log("コールバックの処理");
    });
}

/**
 * アダプターのダミー関数
 * @param requests リクエストの配列
 * @param callBack コールバック関数
 */
async function adapterDammy(requests: RequestDammyModel[], callBack: (responses: ResponseDammyModel[]) => void) {
    console.log("開始");
    let responses: ResponseDammyModel[] = [];
    const execRequests = sliceByNumber<RequestDammyModel>(requests, 3);
    for (let i = 0; i < execRequests.length; i++) {
        await Promise.all(execRequests[i].map(execRequest => {
            return requestDammy(execRequest).then((response) => {
                return response;
            });
        })).then((responseList) => {
            console.log("↓↓↓" + (i + 1) + "回目の処理結果 ==========");
            console.log(responseList);
            console.log("↑↑↑" + (i + 1) + "回目の処理結果 ==========");
            responseList.forEach(item => {
                responses.push(item);
            });
        });
    }
    callBack(responses);
    console.log("終了");
}

/**
 * リクエスト送信を模倣した関数
 * @param requestDammy
 * @returns
 */
function requestDammy(request: RequestDammyModel): Promise<ResponseDammyModel> {
    return new Promise((resoleve) => {
        setTimeout(() => {
            const response: ResponseDammyModel = {
                message: "リクエストID:" + request.requestId + " 実行完了"
            }
            resoleve(response);
        }, request.waitTime * 1000);
    });
}

/**
 * 配列を分割する
 * @param array 
 * @param number 
 * @returns 
 */
function sliceByNumber<T>(array: T[], number: number): T[][] {
    const length = Math.ceil(array.length / number);
    return new Array(length).fill(0).map((_, i) => {
        return array.slice(i * number, (i + 1) * number);
    })
}

/**
 * ダミーリクエスト
 */
interface RequestDammyModel {
    /** リクエストID */
    requestId: string;
    /** 待機時間 */
    waitTime: number
}

/**
 * ダミーレスポンス
 */
interface ResponseDammyModel {
    /** メッセージ */
    message: string;
}

